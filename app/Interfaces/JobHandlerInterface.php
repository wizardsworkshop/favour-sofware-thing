<?php

namespace App\Interfaces;

interface JobHandlerInterface
{
	public function handleJobs();
}