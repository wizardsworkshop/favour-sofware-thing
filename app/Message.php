<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\CanDecrypt;

class Message extends BasicTimestampModel
{
    use CanDecrypt;    

    protected $primaryKey = 'messageId';
    protected $table = 'messages';

    public function sender()
    {
    	return $this->belongsTo('App\Account', 'sender', 'accountId');
    }

    public function recipient()
    {
    	return $this->belongsTo('App\Account', 'recipient', 'accountId');
    }

    public function getBodyAttribute($body)
    {
        return $this->decrypt($body);
    }

    public function setBodyAttribute($body)
    {
        $this->attributes['body'] = encrypt($body);
    }
}
