<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagsPerFavour extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'tags_per_favour';
    public $timestamps = false;

    protected $fillable = ['tagId', 'favourId'];
}
