<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\Tags\TagCacheHandler;

class TagServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('TagCacheHandler', function($app) {
            return new TagCacheHandler;
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
