<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

use App\Traits\CanDecrypt;

class Account extends Model
{
    use CanDecrypt;

    protected $primaryKey = 'accountId';
    protected $table = 'accounts';
    public $timestamps = false;

    public function favours()
    {
        return $this->hasMany('App\Favour', 'accountId', 'accountId');
    }

    public function escrow()
    {
        return $this->hasMany('App\Escrow', 'buyerId', 'accountId');
    }
    
    // MUTATOR encrypts passwords by default in model

    public function setPasswordAttribute($pass)
    {
    	$this->attributes['password'] = Hash::make($pass);
    }

    // Get an account instance by username

    public static function byUser($username)
    {
        return self::where('username', $username)->first();
    }

    public function checkPassword($pass)
    {
    	// Validate login password attempt

    	return Hash::check(
    		$pass,
    		$this->password
    	);
    }
}
