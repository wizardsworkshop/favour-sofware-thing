<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class BasicTimestampModel extends Model
{
    // We want created_at but not updated_at 

    const UPDATED_AT = null;
    const CREATED_AT = 'timestamp';

    public function getTimestampAttribute($timestamp)
    {
    	return new Carbon($timestamp);
    }
}
