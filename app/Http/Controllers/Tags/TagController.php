<?php

namespace App\Http\Controllers\Tags;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Helpers\Tags\TagCacheHandler;

class TagController extends \App\Http\Controllers\Controller
{
	public function __construct(Request $r, TagCacheHandler $tch)
	{
		$this->tch = $tch;
	}

	public function getTagAutocomplete(Request $r)
	{
		return [
			'type' => 'success',
			'tags' => [
				'results' => $this->getSuggestedTags($r->term)
			]
		];
	}

    public function getSuggestedTags($search)
    {
    	return $this->tch->suggestedTags($search, 5);
    }
}
