<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Account;

use App\Helpers\Users\UserManager;

class AuthController extends \App\Http\Controllers\UserController 
{
	public function check(Request $r)
	{
		$account = Account::byUser($r->user);

        if (!$account) {
            return $this->failLogin();
        }

		if ($account->checkPassword($r->pass)) {
	        set_session_account($account);
		
     		return redirect()->route('home');
		}

        return $this->failLogin();
	}

    public function index(Request $r)
    {
        if ($r->session()->has('account')) {
            return redirect()->route('home');
        }

        return view('login', []);
    }

    public function register(Request $r)
    {
    	if ($r->isMethod('post')) {
            $account = $this->createAccount(
                $r->user, 
                $r->password, 
                $r->confirmPassword
            );

            if (gettype($account) === 'string') {

                // Account is an error message, not Account class instance

                return redirect()->route('register')->with('status', $account);
            }

            set_session_account($account);

            return redirect()->route('home');
    	}

        return view('register', [
            'user' => UserManager::getNewUsername()
        ]);
    }

    private function createAccount($user, $pass, $cpass)
    {
    	if ($pass === $cpass) {

    		if (UserManager::validatePassword($pass)) {
                return UserManager::createAccount($user, $pass);
    		}

    		return $this->passwordFailsRequirements;
    	}

    	return $this->passwordNotMatching;
    }

    public function terms(Request $r)
    {
        return view('terms', []);
    }

    public function logout(Request $r)
    {
        $r->session()->flush();
        return redirect('/');
    }

	private function failLogin() 
	{
		return redirect()->route('login')->with('status', 'Login Failed, please check username / password');
	}

	public function makeHash(Request $r)
	{
		return Hash::make($r->param);
	}
}