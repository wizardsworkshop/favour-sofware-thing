<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
	protected $passwordNotMatching = 'Confirmation password does not match';
	protected $passwordFailsRequirements = 'Password does not meet requirements';
}
