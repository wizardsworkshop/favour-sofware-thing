<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Favours\FavourJobHandler;
use App\Helpers\Tags\TagCacheHandler;

use App\Favour;
use App\Tag;
use App\TagsPerFavour;
use App\Escrow;

class FavourController extends \App\Http\Controllers\Controller
{
	public function __construct(FavourJobHandler $fjh, TagCacheHandler $tch)
	{
		$fjh->handleJobs();
		$this->fjh = $fjh;
		$this->tch = $tch;
	}

    public function newFavourForm(Request $r)
	{
		return view('favour.new-favour', [
			'favour' => new Favour
		]);
	}

	public function newFavour(Request $r)
	{
		$f = new Favour;
		$updated = $this->processFavourRequest($r, $f);

		if (!$updated) {
			return redirect()->route('new-favour')->with('status', 'Please enter title / body / tokens');
		}

		return redirect()->route('home')->with('status', 'Favour successful, id [' . $f->favourId . ']');
	}

	public function editFavourForm(Request $r, $id)
	{
		return view('favour.edit-favour', [
			'favour' => Favour::find($id),
			'favourId' => $id
		]);
	}

	public function editFavour(Request $r, $id)
	{
		$f = Favour::find($id);

		if ($f) {
			$this->processFavourRequest($r, $f);
			return 'success';
		}

		return 'error - no favour with this id';
	}

	public function deleteFavour(Request $r, $id) 
	{

		$params = [
			'checkAccount' => session('accountId')
		];

		if (Favour::safeDelete($id, $params)) {
			return 'success';
		};

		return 'error';
	}

	// Process favour update / save request
	// by validating values and assigning

	// Will return true of false based on wether
	// the favour was sucessfully updated

	protected function processFavourRequest(Request &$r, Favour &$f)
	{
		$requiredfields = [
			'title' => 64,
			'body' => 512,
			'tokens' => 4
		];

		$reqcount = 0;

		foreach ($requiredfields as $field => $length) {
			if (isset($r->$field)) {
				$flength =  strlen($r->$field);

				if ($flength > 0 && $flength <= $length) {
					$f->$field = $r->$field;
					$reqcount++;
				}

			}
		}

		// Require title, body and tokens

		if ($reqcount == 3) {
			$f->accountId = session('accountId');
			$f->save();

			TagsPerFavour::where(['favourId' => $f->favourId])->delete();

			$tags = $r->tags;

			foreach ($tags as $tagstr) {

					$flength = strlen($tagstr);

					if ($flength > 0 && $flength <= 16) {

						$tagId = $this->tch->getTagIdIfExists($tagstr);
						if ($tagId) {
							$tag = Tag::find($tagId);
						} else {
							$tag = new Tag;
							$tag->description = $tagstr;
							$tag->save();
						}

						$tpf = new TagsPerFavour;
						$tpf->favourId = $f->favourId;
						$tpf->tagId = $tag->tagId;
						$tpf->save();
					}
			}

			return true;
		}

		return false;
	}

	public function redeemFavour(Request $r, $id)
	{
		$f = Favour::findOrFail($id);

		if ($r->isMethod('post')) {
			$account = session('account');
			
			if ($f->redeem($account)) {
				return 'success';
			};

			return 'error';
		}

		return view('favour.redeem-favour', [
			'favour' => $f
		]);
	}

	public function confirmFavourRecieved(Request $r, $id)
	{
		$favour = Favour::find($id);
		if ($favour) {
			$this->fjh->handleEscrow(
				$favour->escrow
			);

			return 'success';
		}

		return 'error';
	}
}
