<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\UserController;
use App\Helpers\Favours\FavourJobHandler;

use App\Account;
use App\Favour;
use App\TagsPerFavour;
use App\Escrow;

use App\Helpers\Users\UserManager;

class HomeController extends UserController
{
	public function __construct(FavourJobHandler $fjh)
    {
    	$fjh->handleJobs();
    }

	public function index(Request $r)
	{
		$page = $this->getCurrentPage($r);
		$search = $this->getCurrentSearch($r);

		// Favours Per Page

		$fpp = 10;
		$filter = [['redeemed', 0]];

		$favours = Favour::getPage(
			$page, 
			$fpp, 
			$filter, 
			$search
		);

		return view('home.index', [
			'favours' => $favours['data'],
			'page' => $page,
			'maxpages' => $favours['maxpages'],
			'currentUser' => session('accountId'),
			'favourCount' => $favours['fullcount']
		]);
	}

	public function myfavours(Request $r)
	{
		$data = array_merge(
			$this->getUserFavourPage(session('accountId'), $r),
			$this->myAccepted($r)
		);
		
		return view('home.my-favours', $data);
	}

	public function myAccepted(Request $r)
	{
		$page = $this->getCurrentPage($r);
		$accId = session('accountId');

		$escrow = Escrow::select('favourId')->where('buyerId', $accId)
			->get()
			->toArray();

		array_walk($escrow, function(&$e) {
			$e = $e['favourId'];
		});

		$favours = Favour::with('account:accountId,username', 'tags')->find($escrow);

		return [
			'acceptedFavours' => $favours,
			'currentUser' => $accId,
			'acceptedFavourCount' => count($escrow)
		];
	}

	public function account(Request $r)
	{
		if ($r->isMethod('post')) {

			if ($r->filled(['password', 'confirmpassword'])) {

				if ($r->password === $r->confirmpassword) {

					if (UserManager::validatePassword($r->password)) {

						// Update password for session account
						// if all requirements pass

						UserManager::changeCurrentAccountPassword($r->password);

						return redirect()->route('account')->with('status', 'Password Updated');
					}

					return redirect()->route('account')->with('status', $this->passwordFailsRequirements);
				}

				return redirect()->route('account')->with('status', $this->passwordNotMatching);
			}

		}

		return view('home.my-account');
	}

	public function viewUser(Request $r, $id) {
		
		// Redirect to my-favours if page belongs to logged in user

		if ($id == session('accountId')) {
			return redirect()->route('my-favours');
		}

		$acc = Account::findOrFail($id);
		$data = $this->getUserFavourPage($id, $r);
		
		$data['account'] = $acc;

		return view('home.view-user', $data);
	}

	public function deleteAccount(Request $r)
	{
		UserManager::deleteAccount();

        session()->flush();
		return redirect()->route('login')->with('status', 'Account deleted');
	}

	private function getUserFavourPage($id, &$r)
	{
		$page = $this->getCurrentPage($r);
		$search = $this->getCurrentSearch($r);

		$fpp = 5;
		$filter = [['accountId', $id]];

		$favours = Favour::getPage(
			$page, 
			$fpp, 
			$filter, 
			$search
		);

		return [
			'favours' => $favours['data'],
			'page' => $page,
			'maxpages' => $favours['maxpages'],
			'favourCount' => $favours['fullcount'],
			'search' => $search
		];
	}

	// Gets current page number based on request

	// Passing request by reference improves performance

	private function getCurrentPage(Request &$r)
	{
		$page = 0;

		if ($r->has('page') && (filter_var($r->page, FILTER_VALIDATE_INT) !== false)) {
			$page = $r->page;
		}

		return $page;
	}

	private function getCurrentSearch(Request &$r)
	{
		$search = [];

		if ($r->has('search')) {
			$search['tag'] = explode(',', $r->search);
		}

		if ($r->has('textsearch')) {
			$search['text'] = $r->textsearch;
		}

		if (count($search) > 0) {
			return $search;
		}

		return null;
	}

}
