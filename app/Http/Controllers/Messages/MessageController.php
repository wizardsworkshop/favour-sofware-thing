<?php

namespace App\Http\Controllers\Messages;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;

use App\Message;
use App\Account;

use App\Helpers\Messages\MessageHandler;

class MessageController extends Controller
{
    public function __construct()
    {
        MessageHandler::handleJobs();
    }

    public function inbox(Request $r)
    {
        $accId = session('accountId');
    	$messages = MessageHandler::getInbox($accId)->toArray();

        return view('messages.inbox', [
            'messages' => $messages,
            'messageCount' => count($messages) 
        ]);
    }

    public function newMessage(Request $r, $recipient)
    {
        $rec = Account::findOrFail($recipient);
    	return view('messages.new-message', [
            'recipient' => $rec
        ]);
    }

    public function sendMessage(Request $r, $recipient)
    {
        if ($r->filled('body') && strlen($r->body) < 512) {

            $sent = MessageHandler::sendMessage(
                $recipient, 
                $r->body, 
                session('accountId')
            );

            if ($sent) {
                return 'success';
            }

        }

        return 'error';
    }
}
