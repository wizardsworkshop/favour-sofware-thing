<?php

namespace App\Http\Middleware;

use Closure;
use App\Account;

class LoggedIn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($r, Closure $next)
    {
        // Checks session contains account and account is instance of Account class

        if ($r->session()->has('account')) {
            $account = session('account');

            if ($account instanceof Account) {
                
                // If we can validate login, we refresh the account in session
                // to account for any token changes etc.
                
                $r->session()->put('account', Account::find($account->accountId));
                return $next($r);
            }
        }

        return redirect()->route('login')->with('status', 'session expired');
    }
}
