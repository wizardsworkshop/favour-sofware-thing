<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Escrow extends BasicTimestampModel
{
	protected $primaryKey = 'id';
    protected $table = 'escrow';

    protected $fillable = ['favourId', 'buyerId'];

    public function favour() 
    {
    	return $this->belongsTo('App\Favour', 'favourId', 'favourId');
    }

    public function account() 
    {
    	return $this->belongsTo('App\Account', 'accountId', 'buyerId');
    }
}
