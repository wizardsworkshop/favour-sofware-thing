<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\Traits\CanDecrypt;
use App\Escrow;
use App\TagsPerFavour;
use App\Helpers\Tags\TagCacheHandler;

class Favour extends BasicTimestampModel
{
    use CanDecrypt;

    protected $primaryKey = 'favourId';
    protected $table = 'favours';

    // Relationships
    
    public function account() 
    {
    	return $this->belongsTo('App\Account', 'accountId', 'accountId');
    }

    public function tags()
    {
    	return $this->hasManyThrough(
    		'App\Tag',
    		'App\TagsPerFavour',
    		'favourId',
    		'tagId',
    		'favourId',
    		'tagId'
    	);
    }

    public function escrow() 
    {
        return $this->hasOne('App\Escrow', 'favourId', 'favourId');
    }

    // Encrypt / Decrypt attributes with
    // accessors and mutators 

    public function getTitleAttribute($title)
    {
        return $this->decrypt($title);
    }

    public function setTitleAttribute($title)
    {
        $this->attributes['title'] = encrypt($title);
    }

    public function getBodyAttribute($body)
    {
        return $this->decrypt($body);
    }

    public function setBodyAttribute($body)
    {
        $this->attributes['body'] = encrypt($body);
    }

    public function redeem(&$account)
    {
        DB::beginTransaction();
        if ($account->tokens >= $this->tokens) {
            try {
                
                $escrow = new Escrow;
                $escrow->buyerId = $account->accountId;
                $this->escrow()->save($escrow);

                $account->tokens -= $this->tokens;
                $this->redeemed = 1;
                $account->save();
                $this->save();

                DB::commit();
                return true;

            } catch (Exception $ex) {
                
                DB::rollBack();
                if (env('APP_ENV') == 'local') {
                    die(var_dump($ex));
                }

                return false;

            }
        }

        return false;
    }

    // Get a page of favours which can be rendered
    // into a blade favour panel module

    // Returns full count of dataset before 
    // pagination and a derived max page number
    // for the page button controls (next / prev)

    // Takes number of current page (0 indexed) and total
    // numbers of favours per page (fpp) with optional
    // search parameter for body / title

    public static function getPage($page, $fpp, $where = [], $search = '')
    {
        $data = self::with('account:accountId,username', 'tags')
            ->where($where);

        if ($search !== '' && $search !== null) {
            self::applySearch($data, $search);
        }

        $fullcount = $data->count();
        $maxpages = floor($fullcount / $fpp);

        $data = $data->orderBy('favourId', 'desc')
            ->take($fpp)
            ->offset($page * $fpp)
            ->get();

        return [
            'data' => $data,
            'fullcount' => $fullcount,
            'maxpages' => $maxpages
        ];
    }

    public static function applySearch(&$builder, array $search)
    {
        if (isset($search['tag'])) {
            $tch = new TagCacheHandler;

            $tagIds = $tch->getAllTagIds($search['tag']);

            $tpf = TagsPerFavour::select('favourId')->whereIn('tagId', $tagIds)->get();

            $ids = [];

            foreach ($tpf as $t) {
                if (!in_array($t->favourId, $ids)) {
                    $ids[] = $t->favourId;
                }
            }

            $builder->whereIn('favourId', $ids);
        }

        if (isset($search['text'])) {

        }
    }

    public static function safeDelete($id, $params = [])
    {
        $favour = Favour::find($id);
        if ($favour) {
            if (isset($params['checkAccount'])) {
                if ($params['checkAccount'] != $favour->accountId) {
                    return false;
                }
            }

            TagsPerFavour::where(['favourId' => $favour->favourId])->delete();
            $data = [
                'tokens' => $favour->tokens,
                'accountId' => $favour->accountId
            ];

            $favour->delete();
            return $data;
        }

        return false;
    }
}
