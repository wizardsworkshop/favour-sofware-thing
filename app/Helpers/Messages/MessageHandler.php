<?php

namespace App\Helpers\Messages;

use Carbon\Carbon;

use App\Message;

class MessageHandler
{
	public static function handleJobs()
	{
		// Clear messages more than 3 hours ago

		Message::where('timestamp', '<', Carbon::now()->subHours(3))->delete();
	}

	public static function getInbox($accId)
	{
		// Get full inbox for a given account ID

		return Message::with('sender')->where(['recipient' => $accId])->get();
	}

	/**
	 * Send a standard message
	 *
	 * @param int recipient  Account ID Recieving the message
	 * @param string body    Body of the message
	 * @param int sender     Optional Account ID of sender,
	 *						 default 0 will mean a system message
	 */ 

	public static function sendMessage(int $recipient, string $body, int $sender = 0)
	{
		$m = new Message;
		$m->recipient = $recipient;
		$m->body = $body;
		$m->sender = $sender;
		return $m->save();
	}
}