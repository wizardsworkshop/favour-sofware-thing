<?php

namespace App\Helpers\Users;

use App\Account;

class UserManager
{
	public static function getNewUsername()
    {
        // 10 character random string

        return substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"), 0, 10);
    }

    public static function validatePassword($pass)
    {
    	// Between 8 - 32 characters, 1 uppercase, 1 lowercase
    	// and 1 number. Allows special characters.

    	return (preg_match('^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,32}$^', $pass) === 1);
    }

    public static function createAccount($username, $pass)
    {
    	$account = new Account;
        $account->username = $username;
        $account->password = $pass;
        $account->save();
        return $account;
    }

    public static function changeCurrentAccountPassword($newpass)
    {
    	$account = session('account');
		$account->password = $newpass;
		$account->save();
    }

    public static function deleteAccount($accountId = null)
    {
        $accId = $accountId ?? session('accountId');
        $account = Account::findOrFail($accId);
        foreach ($account->favours as $favour) {
            // Delete all favours / tags per favour
            // by the user to delete

            TagsPerFavour::where('favourId', $favour->favourId)->delete();
            $favour->delete();
        }

        $account->delete();
    }
}