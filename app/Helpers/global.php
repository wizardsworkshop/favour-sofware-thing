<?php

use App\Account;

function set_session_account(Account $account)
{
	session([
        'account' => $account,

        // PERFORMANCE

        'accountId' => $account->accountId
    ]);
}