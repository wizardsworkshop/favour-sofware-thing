<?php

namespace App\Helpers\Favours;

use App\Interfaces\JobHandlerInterface;

use App\Favour;
use App\Escrow;
use App\Account;
use Carbon\Carbon;

class FavourJobHandler implements JobHandlerInterface
{
	public function handleJobs()
	{
		$n = Carbon::now();

		// We need to delete favours more than a week old

		Favour::where('timestamp', '<', $n->copy()->subWeek())->delete();
		
		// We need to autocomplete escrow after 24 hours

		$escrow = Escrow::where('timestamp', '<', $n->copy()->subDay())->get();

		foreach ($escrow as $e) {
			$this->handleEscrow($e);
		}
	}

	public function handleEscrow(Escrow &$e)
	{
		$fData = Favour::safeDelete($e->favourId);
		if ($fData) {
			$a = Account::find($fData['accountId']);
			$a->tokens += $fData['tokens'];
			$a->save();
			$e->delete();
		}
	}
}