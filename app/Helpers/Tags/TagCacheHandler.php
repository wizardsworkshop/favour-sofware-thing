<?php

namespace App\Helpers\Tags;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use App\Traits\CanDecrypt;

class TagCacheHandler 
{
	use CanDecrypt;

	public $tags = false;

	public function getTagCache()
	{
		if (Cache::has('suggestedtags')) {
			$tags = Cache::get('suggestedtags');
		} else {
			$query = '
				SELECT 
					t.tagId, 
					t.description, 
					COUNT(tpf.tagId) AS count
				FROM 
					tags t
				INNER JOIN
					tags_per_favour tpf
						ON t.tagId = tpf.tagId
				GROUP BY 
					t.description, 
					t.tagId
				ORDER BY 
					COUNT(tpf.tagId) DESC
			';

			$results = DB::select(DB::raw($query));
			$tags = [];

			foreach ($results as $row) {
				$desc = $this->decrypt($row->description);
				$tags[$desc] = [
					'display' => $desc . ' (' . $row->count . ')',
					'id' => $row->tagId
				];
			}

			Cache::set('suggestedtags', $tags, now()->addSeconds(5));
		}

		return $tags;
	}


	public function suggestedTags($search, $limit)
	{
		$tags = $this->getTagInstance();

		$searchlength = strlen($search);

		$results = [];
		$resultCount = 0;

		foreach ($tags as $desc => $info) {
			if (substr($desc, 0, $searchlength) == $search) {
				$results[] = [
					'id' => $desc, 
					'text' => $info['display']
				];

				if (++$resultCount == $limit) {
					break;
				};
			} 
		}

		return $results;
	}

	public function getTagIdIfExists(String $desc)
	{
		$tags = $this->getTagInstance();

		if (array_key_exists($desc, $tags)) {
			return $tags[$desc]['id'];
		}

		return false;
	}

	public function getAllTagIds(Array $search)
	{
		$tags = $this->getTagInstance();
		$ids = [];

		foreach ($search as $tag) {
			$id = $this->getTagIdIfExists($tag);
			
			if ($id) {
				$ids[] = $id;
			}
		}

		return $ids;
	}

	private function getTagInstance()
	{
		if (!$this->tags) {
			$this->tags = $this->getTagCache();
		}

		return $this->tags; 
	}

	// Find tags used by favours
	// with a tag in common with
	// those in the array of IDs

	public function relatedTags(Array $tagIds)
	{
		$query = '
			SELECT 
				tagId 
			FROM 
				tags_per_favour
			WHERE 
				favourId IN (
					SELECT 
						favourId
					FROM
						tags_per_favour
					WHERE 
						tagId IN (__TAGSEARCH__)
				) AND 
				tagId NOT IN (__TAGSEARCH__)
			GROUP BY 
				tagId
			ORDER BY 
				COUNT(tagId) desc
		';

		$query = str_replace('__TAGSEARCH__', implode(',', $tagIds), $query);

		return DB::select($query);
	}

}