<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\CanDecrypt;

class Tag extends Model
{
    use CanDecrypt;

    protected $primaryKey = 'tagId';
    protected $table = 'tags';
    public $timestamps = false;

    protected $fillable = [
    	'description'
    ];

    public function getDescriptionAttribute($description)
    {
        if (strlen($description) > 0) {
            return $this->decrypt($description);
        }

        return $description;
    }

    public function setDescriptionAttribute($description)
    {
        $this->attributes['description'] = encrypt($description);
    }

    public function favours()
    {
    	return $this->hasManyThrough(
    		'App\Favour',
    		'App\TagsPerFavour',
    		'tagId',
    		'favourId',
    		'tagId',
    		'favourId'
    	);
    }
}
