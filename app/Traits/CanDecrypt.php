<?php

namespace App\Traits;

use Illuminate\Contracts\Encryption\DecryptException;

// Basic wrapper trait for native decrypt function
// Catch exception for invalid payload

trait CanDecrypt
{
    protected function decrypt($value)
    {
        if (strlen($value) > 0) {
            try {

                return decrypt($value);

            } catch (DecryptException $ex) {
                if (env('APP_ENV') === 'local') {
                    die(print_r($ex, true));
                }

                die;
            }
        }
    }
}