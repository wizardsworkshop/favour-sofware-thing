<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\LoggedIn;

/**
 * Login Logout
 */

Route::namespace('Auth')->group(function() {
	Route::get('/', 'AuthController@index')->name('login');
	Route::get('login', 'AuthController@index');
	Route::get('logout', 'AuthController@logout');
	Route::post('login', 'AuthController@check');
	
	Route::any('register', 'AuthController@register')->name('register');
	Route::get('terms', 'AuthController@terms');
});

/**
 * Internal Pages
 */

// id parameter is validated globally to be numeric
// see: RouteServiceProider.php

Route::prefix('home')->namespace('Home')->middleware(LoggedIn::class)->group(function() {
	Route::get('index', 'HomeController@index')->name('home');
	
	Route::get('account', 'HomeController@account')->name('account');
	Route::post('account', 'HomeController@account');
	Route::get('my-favours', 'HomeController@myfavours')->name('my-favours');
	Route::get('my-accepted', 'HomeController@myAccepted')->name('my-accepted');
	Route::get('user/{id}', 'HomeController@viewUser');

	Route::get('deleteAccount', 'HomeController@deleteAccount');
});

Route::prefix('favour')->namespace('Home')->middleware(LoggedIn::class)->group(function() {
	Route::get('new-favour', 'FavourController@newFavourForm')->name('new-favour');
	Route::get('edit-favour/{id}', 'FavourController@editFavourForm')->name('edit-favour');

	Route::post('new-favour/submit', 'FavourController@newFavour');
	Route::post('edit-favour/{id}', 'FavourController@editFavour');
	Route::post('delete-favour/{id}', 'FavourController@deleteFavour');

	Route::get('redeem-favour/{id}', 'FavourController@redeemFavour');
	Route::post('redeem-favour/{id}', 'FavourController@redeemFavour');

	Route::post('confirm-favour/{id}', 'FavourController@confirmFavourRecieved');
});

Route::prefix('messages')->namespace('Messages')->middleware(LoggedIn::class)->group(function() {
	Route::get('inbox', 'MessageController@inbox');

	// id = Recipient id
	Route::get('new-message/{id}', 'MessageController@newMessage');
	Route::post('send-message/{id}', 'MessageController@sendMessage');

});

Route::prefix('tags')->namespace('Tags')->group(function() {
	Route::get('tag-autocomplete', 'TagController@getTagAutocomplete');
});
