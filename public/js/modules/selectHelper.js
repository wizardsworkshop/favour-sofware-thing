var initTagAutocomplete = function(allowNew) {
	$('.tag-autocomplete-select').select2({
		placeholder: 'Tags',
		maximumSelectionLength: 3,
		tags: allowNew,
		ajax: {
			url: '/tags/tag-autocomplete',
			dataType: 'json',
			delay: 450,
			processResults: function(data) {
				return data.tags;
			}
		}
	});
}