var checkFields = function(fields) {
	for (var i = 0; i < fields.length; i++) {
		var currentField = fields[i];
		if (currentField.type === 'checkbox') {
			if (!currentField.checked) {
				swal('you must check the checkbox: ' + currentField.placeholder);
				return false;
			}
		} else if (typeof currentField === 'object') {
			if (currentField.value === '') {
				swal('you must complete all required fields');
				return false;
			}
		} else {
			if (currentField.value.length === 0) {
				swal('you need to fill in field: ' + currentField.placeholder);
				return false;
			}
		}
	}
	return true;
}	