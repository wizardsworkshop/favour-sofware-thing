var deleteWarning = function(cb, msg) {
	var message = 'This action is irreversable';

	if (typeof msg !== 'undefined') {
		message = msg;
	}

	swal(
		'Are you sure?', 
		message, 
		'question'
	).then((value) => {
		if (value !== null) {
			cb();
		}
	});
}