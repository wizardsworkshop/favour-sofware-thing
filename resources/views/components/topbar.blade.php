<div class="row topbar">
	<div class="col-sm-12">
		<a href="/home/index" class='btn btn-default'>Home <span class="glyphicon topbar-glyph glyphicon-home"></span></a>

		<a href="/home/account" class='btn btn-default'>My Account <span class="glyphicon topbar-glyph glyphicon-user"></span></a>

		<a href="/home/my-favours" class='btn btn-default'>My Favours <span class="glyphicon topbar-glyph glyphicon-th-list"></span></a>

		<a href="/messages/inbox" class='btn btn-default' data-toggle="tooltip" data-placement="top" title="Tooltip on top">My Inbox <span class="glyphicon topbar-glyph glyphicon-book"></span></a>

		<a href="/logout" class='btn btn-default'>Logout <span class="glyphicon topbar-glyph glyphicon-remove"></span></a>
	</div>
	{{ $slot }}
</div>		