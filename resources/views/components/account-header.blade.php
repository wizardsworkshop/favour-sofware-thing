<h3>Username</h3>
{{ $account->username }} 
<h3>Tokens</h3>
{{ $account->tokens . ($account->tokens !== 1 ? ' tokens' : ' token') }}
@if($account->accountId !== session('accountId'))
	<a class="btn btn-default" style="margin: 10px;" href="/messages/new-message/{{ $account->accountId }}">message <span class="glyphicon glyphicon-envelope"></span></a>
@endif
