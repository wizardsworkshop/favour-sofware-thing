<div class="panel panel-default">
	<div class="panel-heading">
		<a href="/home/user/{{ $message['sender']['accountId'] }}"><span class="glyphicon glyphicon-envelope"></span> {{ $message['sender']['username'] }}</a>
		<span style="float: right;">
			<a class="btn btn-default reply-button" href="/messages/new-message/{{ $message['sender']['accountId'] }}">Reply</a>
		</span>
	</div>

	<div class="panel-body">
		{{ $message['body'] }}
	</div>

	<li class="list-group-item">
		{{ $message['timestamp'] }}
	</li>
</div>