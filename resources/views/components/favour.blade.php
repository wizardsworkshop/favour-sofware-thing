<div class="panel panel-default" id="favour{{ $favour['favourId'] }}">
  <div class="panel-heading">
  	<strong>[{{ $favour['favourId'] }}]</strong> {{ $favour['title'] }}
  </div>

  <div class="panel-body">
    <p>{{ $favour['body'] }}</p>
  </div>

  <ul class="list-group">
  	<li class="list-group-item">
  		<br />

      {{--
        If the favour belongs to the user,
        we show the link to their profile (my-favours)
      --}}

      <a href="{{ $belongsToUser ? 'my-favours' : ('/home/user/' . $favour['account']['accountId'] ) }}">Posted By: {{ $favour['account']['username'] }}</a> <br /> {{ $favour['timestamp']->format('d/m/Y') }} {{ $favour['timestamp']->format('h:i:s') }}
  	</li>
  	
    {{--
      Output each tag if tags exist
    --}}

  	@if(!empty($favour['tags']))
	    <li class="list-group-item">
	    	@foreach($favour['tags'] as $tag)
	    		<span class="label label-default"><a href="/home/index?search={{ $tag['description'] }}" >{{ $tag['description'] }}</a></span>
	    	@endforeach
	    </li>
    @endif

    @if($belongsToUser)

      @if($favour->escrow !== null)

      {{--
        Redeemed and owned, show time
        redeemed for auto timeout
      --}}

        <li class="list-group-item">
          {{ $favour->tokens }} {{ intval($favour->tokens) > 1 ? 'tokens' : 'token' }}
          Reedemed {{ $favour->escrow->timestamp->format('d/m/Y H:m:s') }}
        </li>

      @else

      {{--
        Not redeemed but owned, so show
        options to remove and edit
      --}}

        <li class="list-group-item">
          {{ $favour->tokens }} {{ intval($favour->tokens) > 1 ? 'tokens' : 'token' }}
          <a href="/favour/edit-favour/{{ $favour['favourId'] }}" class="btn btn-default change-listing edit-listing">Edit</a>
          <button class="btn btn-danger change-listing remove-listing" rel="{{ $favour['favourId'] }}">Remove</button>
        </li>

      @endif
    
    @elseif($favour->escrow !== null)

    {{--
      Favour redeemed by logged in user
      Show escrow confirm button to release tokens
     --}}

      <li class="list-group-item">
        {{ $favour->tokens }} {{ intval($favour->tokens) > 1 ? 'tokens' : 'token' }}
        <button rel="{{ $favour['favourId'] }}" class="btn btn-default change-listing confirm-listing">Confirm</button>
        Reedemed {{ $favour->escrow->timestamp->format('d/m/Y H:m:s') }}
      </li>

    @else

    {{--
      Favour not owned by user and not redeemed
    --}}

      <li class="list-group-item">
        {{ $favour->tokens }} {{ intval($favour->tokens) > 1 ? 'tokens' : 'token' }}
        <a href="/favour/redeem-favour/{{ $favour['favourId'] }}" class="btn btn-primary change-listing redeem-listing">Accept Favour</a>
      </li>

    @endif
   	
  {{-- Include extra list-group-item here --}}
	{{ $slot }}
  
  </ul>
</div>