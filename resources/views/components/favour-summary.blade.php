<div class="favour-info">
	<h2>
		<strong>[{{ $favour->favourId }}] {{ $favour->account->username }}</strong>
		<p style="margin: 20px;">{{ $favour->title }}</p>
	</h2>
	{{-- I know you're not meant to make it add up to 14 its meant to be 12 but it works, ok? --}}
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-12">
			{{ $favour->body }}
		</div>
	</div>
	<div style="padding-top: 11px; font-size: 20px;" class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-12">
			Token Cost: {{ $favour->tokens }} {{ intval($favour->tokens) > 1 ? 'tokens' : 'token' }}
		</div>
	</div>
</div>