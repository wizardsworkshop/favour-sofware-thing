{{ csrf_field() }}
<div class="form-group row">
	<div class="col-lg-1">
		<label for="title">Title</label>
	</div>
	<div class="col-lg-11">
		<input type="text" class="form-control favour-form" id="title" name="title" maxlength="64" value="{{ $favour['title'] }}" placeholder="64 character max" />
	</div>
</div>
<div class="form-group row">
	<div class="col-lg-1">
		<label for="body">Body</label>
	</div>
	<div class="col-lg-11">
		<textarea class="form-control favour-form" id="body" name="body" maxlength="512" placeholder="512 character max" style="height: 14rem;">{{ $favour['body'] }}</textarea>
	</div>
</div>
<div class="form-group row">
	<div class="col-lg-1">
		<label for="tokens">Tokens</label>
	</div>
	<div class="col-lg-11">
		<input type="number" class="form-control favour-form" id="tokens" name="tokens" maxlength="64" value="{{ $favour['tokens'] }}" placeholder="Cost of the favour" />
	</div>
</div>
	<div class="form-group row">
		<div class="col-lg-1">
			<label for="tags">Tags</label>
		</div>
		<div class="col-lg-11">
			<select multiple class="form-control select2 tag-autocomplete-select" id="tag-input" name="tags[]">
				@foreach($favour['tags'] as $tag)
					<option selected value="{{ $tag['description'] }}">{{ $tag['description'] }}</option>
				@endforeach
			</select>
		</div>
	</div>
</div>