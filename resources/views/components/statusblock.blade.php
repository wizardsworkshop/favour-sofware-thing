@if(session('status'))
    <blockquote>
        <p>{{ session('status') }}</p>
    </blockquote>
    {{ $slot }}
@endif