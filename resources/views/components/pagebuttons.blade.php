<div class="pageButtons">
	<div class="pageBack"><button class="btn btn-default pagebtn" rel="{{ $page == 0 ? -1 : ($page - 1) }}">< Prev</button></div>
	<div>{{ $slot }}</div>
	<div class="pageNext"><button class="btn btn-default pagebtn" rel="{{ $page == $maxpages ? -1 : ($page + 1) }}">Next ></button></div>
</div>