@extends('layouts.layout')
@section('head')
	@push('extrahead')
		<script type="text/javascript">
			$(function() {
				initTagAutocomplete(true);

				$('#favourForm').submit(function(e) {
					e.preventDefault();
					var data = {
						tags: []
					};

					$.each($(this).serializeArray(), function(i, field) {
						if (field.name === 'tags[]') {
							data['tags'][data['tags'].length] = field.value
						} else {
							data[field.name] = field.value;
						}
					});

					$.post('/favour/edit-favour/{{ $favourId }}', data, function(res) {
						if (res === 'success') {
							swal(
								'success',
								'favour updated',
								'success'
							);
						}
					});
				});
			});
		</script>
	@endpush
	@parent
	<h2 align="center">Edit Favour [{{ $favourId }}]</h2>
	<form method="POST" id="favourForm" action="/favour/edit-favour/{{ $favourId }}">
		@component('components.favour-form', [
			'favour' => $favour
		])
		@endcomponent
		<div style="text-align: center;margin:10px 0px;">
			<input type="submit" class="btn btn-default btn-lg" style="width: 150px;" name="submit" value="Save" />
		</div>
	</form>
@endsection