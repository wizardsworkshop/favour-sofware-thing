@extends('layouts.layout')
@section('head')
	@push('extrahead')
	<script type="text/javascript">
		$(function() {
			initTagAutocomplete(true);
		});
	</script>
	@endpush
	@parent

	<h2 align="center">New Favour</h2>

	@component('components.statusblock')
	@endcomponent

	<form method="POST" action="/favour/new-favour/submit">
	@component('components.favour-form', [
		'favour' => $favour
	])
	@endcomponent
		<div style="text-align: center;margin:10px 0px;">
			<input type="submit" class="btn btn-default btn-lg" style="width: 150px" name="submit" value="Save" />
		</div>
	</form>
@endsection
