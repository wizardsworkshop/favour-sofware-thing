@extends('layouts.layout')
@section('head')
	@push('extrahead')
		<script type="text/javascript">
			$(function() {
				$('.redeem-favour-confirm').click(function(e) {
					e.preventDefault();
					$.post('/favour/redeem-favour/{{ $favour->favourId }}', function(res) {
						if (res == 'success') {
							swal('favour accepted');
						} else {
							swal('you don\'t have enough tokens');
						}
					});
				})
			});
		</script>
		<style type="text/css">
			.redeem-favour {
				font-size: 16px;
			}

			.favour-info {
				border: 1px;
			    border-radius: 15px;
			    padding: 10px;
			    border-opacity: 0.3;
			    box-shadow: 0px 0px 0px 1px rgba(0,0,0,0.3);
			}

			.favour-info > h2 {
				margin: 5px;
			}
		</style>
	@endpush
	@parent
	<div class="redeem-favour">
		@component('components.favour-summary', [
			'favour' => $favour
		])
		@endcomponent
		<div class="row" style="padding-top: 10px; padding-bottom: 10px;">
			<div class="col-sm-2"></div>
			<div class="col-sm-12" style="font-size: 16px;">
				Are you sure you want to accept this favour?
				<button class="btn btn-default redeem-favour-confirm" style="margin: 10px;">Accept</button>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-12">
				<p>When the favour is redeemed, your tokens will go into escrow.</p>
				<p>they will be released to the recipient when you confirm the favour is complete,
				or after a period of 24 hours.</p>
			</div>
		</div>
	</div>
@endsection