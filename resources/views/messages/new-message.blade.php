@extends('layouts.layout')
@section('head')
	@push('extrahead')
	<script type="text/javascript">
		$(function() {
			$('#send-message').click(function() {

				var body = $('#body').val();
				if (!body || body.length < 1 || body.length > 512) {
					swal('message must be between 1 and 512 characters');
					return;
				}

				deleteWarning(function() {
					$.post('/messages/send-message/{{ $recipient->accountId }}', {body: body}, function(res) {
						console.log(res);
					});
				}, 'Are you certain you want to send this message?');
			});
		});
	</script>
	@endpush
	@parent
	<a href="/messages/inbox" class="btn btn-default">< back</a>
	<div class="form-group" style="text-align: left;">
		<h3>New Message</h3>
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-12"><h4>To: {{ $recipient->username }}</h4></div>
		</div>
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-12"><label for="body">Body</label></div>
		</div>
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-12"><textarea name="body" id="body" class="form-control"></textarea></div>
		</div>
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-12">
				<button class="btn btn-default" id="send-message" style="margin-top: 10px;">Send <span class="glyphicon glyphicon-envelope"></span></button>
			</div>
		</div>
	</div>
@endsection