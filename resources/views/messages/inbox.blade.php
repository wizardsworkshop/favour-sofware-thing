@extends('layouts.layout')
@section('head')
	@push('extrahead')
	
	@endpush
	@parent
	@if($messageCount == 0)
		<div style="text-align: center;">
			<h2>You have no messages</h2>
		</div>
	@else
		<div style="text-align: left;">
			@foreach($messages as $message)
				@component('components.message', [
					'message' => $message
				])
				@endcomponent
			@endforeach
		</div>
	@endif
@endsection