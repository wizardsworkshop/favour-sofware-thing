@extends('layouts.layout')
@section('head')
	@parent
    <div class='row'>
        <div class='col-sm-4 col-md-offset-4'>
            <form method='POST' name='loginForm' action='/login'>
                @component('components.statusblock')
                @endcomponent

                {{ csrf_field() }}
                
                <div class='form-group row'>
                    <div class='col-sm-12'>
                        <input name='user' type='text' class='form-control' id='user' placeholder='username'>
                    </div>
                </div>
                <div class='form-group row'>
                    <div class='col-sm-12'>
                        <input name='pass' type='password' class='form-control' id='inputPassword' placeholder='password'>
                    </div>
                </div>

                <div class='form-group row'>
                    <div class='col-sm-8'>
                        <a href='/register'>Register</a> | <a href='/terms'>Terms</a>
                    </div>
                    <div class='col-sm-4'>
                        <div class='text-right'>
                            <button name="bLogin" onclick="event.preventDefault(); if (checkFields([document.loginForm.user, document.loginForm.pass])) {document.loginForm.submit();}" class="btn btn-default" id="bLogin">Login</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection