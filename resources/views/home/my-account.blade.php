@extends('layouts.layout')
@section('head')
	@push('extrahead')
	<script type="text/javascript">
		$(function() {
			$('.delete-account').click(function(e) {
				e.preventDefault();
				deleteWarning(function() {
					window.location.href = '/home/deleteAccount';
				});
			})
		});
	</script>
	<style type="text/css">
		.reset-pass-form {
			margin: auto;
		}

		.reset-pass-form * {
			margin: 10px auto;
		}

		.form-control {
			text-align: left;
		}
	</style>
	@endpush
	@parent

	@component('components.account-header', [
		'account' => session('account')
	])
	@endcomponent

	<div style="text-align: left;">
		<h3>Change Password</h3>
		<form method="POST">
			{{ csrf_field() }}
			<div class="row" style="padding: 0px 15px;">
				<div class="form-group col reset-pass-form">
					<label for="password">Password:</label>
					<input class="form-control" type="password" name="password" id="password"/>
					<label for="confirmpassword">Confirm password:</label>
					<input class="form-control" type="password" name="confirmpassword" id="confirmpassword"/>
					<input class="btn btn-default" type="submit" name="submit-pass-reset" value="Update" />
				</div>
			</div>
		</form>
		<hr />
		<p>Delete Account</p>
		<button type="button" class="btn btn-danger delete-account">Continue <span class="glyphicon glyphicon-remove"></span></button>
	</div>
@endsection