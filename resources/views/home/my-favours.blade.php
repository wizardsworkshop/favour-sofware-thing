@extends('layouts.layout')
@section('head')
	@push('extrahead')
		<script type="text/javascript">
			
		</script>
		<style type="text/css">
			.panel-heading {
				font-size: 2rem;
			}
		</style>
	@endpush
	@parent

	<h3>Accepted Favours</h3>
	<hr />
		@if($acceptedFavourCount > 0)
		    <div>
		    	<div style="text-align: left;">

			    	@foreach($acceptedFavours as $fav)
						@favour([
							'favour' => $fav,
							'belongsToUser' => false
						])
						@endfavour
			    	@endforeach

		    	</div>
		    </div>
		@else
			<div style="text-align: center; padding-top: 5rem;">
				<h4>You have no accepted favours</h4>
				<h4>Look for favours <a href="/home/index" style="text-decoration: underline;">here</a></h4>
			</div>
	    @endif
	<h3>My Favours</h3>
	<hr />
	@if($favourCount > 0)
		<div class="toppagebuttonwrapper">
		    @component('components.pagebuttons', [
		   		'page' => $page,
		    	'maxpages' => $maxpages
		    ])
		    <div align="center" style="text-align: center;">
		    	<a class="btn btn-default" href="/favour/new-favour">New Favour</a>
		    </div>
			@endcomponent
	    </div>
	    <div>
	    	<div style="text-align: left;">

		    	@foreach($favours as $fav)
					@favour([
						'favour' => $fav,
						'belongsToUser' => true
					])
					@endfavour
		    	@endforeach

	    	</div>
	    	@component('components.pagebuttons', [
	    		'page' => $page,
	    		'maxpages' => $maxpages
	    	])
		    @endcomponent
	    </div>
	@else
		<div style="text-align: center; padding-top: 5rem;">
			<h4>You have no favours</h4>
			<h4>Post a new favour <a href="/favour/new-favour" style="text-decoration: underline;">here</a></h4>
		</div>
    @endif
@endsection