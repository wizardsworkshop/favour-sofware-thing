@extends('layouts.layout')
@section('head')
	@push('extrahead')
		<script type="text/javascript">
			
		</script>
		<style type="text/css">
			.panel-heading {
				font-size: 2rem;
			}

			.pageButtons {
				margin-bottom: 22px;
			}
		</style>
	@endpush
	@parent

	@component('components.account-header', [
		'account' => $account
	])
	@endcomponent

	@if($favourCount > 0)
		<div class="toppagebuttonwrapper">
		    @component('components.pagebuttons', [
		   		'page' => $page,
		    	'maxpages' => $maxpages
		    ])
			@endcomponent
	    </div>
	    <div>
	    	<div style="text-align: left;">

		    	@foreach($favours as $fav)
					@favour([
						'favour' => $fav,
						'belongsToUser' => false
					])
					@endfavour
		    	@endforeach

	    	</div>
	    	@component('components.pagebuttons', [
	    		'page' => $page,
	    		'maxpages' => $maxpages
	    	])
		    @endcomponent
	    </div>
	@else
		<div style="text-align: center; padding-top: 5rem;">
			<h4>This user currently has no favours</h4>
		</div>
    @endif
@endsection