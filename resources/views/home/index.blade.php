@extends('layouts.layout')
@section('head')
	@push('extrahead')
		<script type="text/javascript">
			$(function() {
				initTagAutocomplete(false);

				$('.toggleSearch').click(function() {
					$('.searcharea').toggle();
				});

				$('.submit-search').click(function() {
					var search = $('#tag-search-input').val();

					var url = new URL(location.href);
					url.searchParams.set('search', search);
					location.href = url.href;
				});
			});
		</script>
		<style type="text/css">
			.panel-heading {
				font-size: 2rem;
			}

			.searcharea {
				display: none;
				text-align: center;
			}

			.searcharea select {
				width: 40rem;
			}

			#text-search-input {
				width: 40rem;
    			margin: 10px auto;
    			background-color: white;
    			border: 1px solid #ccd0d2;
			}

			.submit-search {
				width: 20rem;
				margin: 10px auto auto auto;
			}

		</style>
	@endpush
	@parent
	<div class="toppagebuttonwrapper">
	    @component('components.pagebuttons', [
	   		'page' => $page,
	    	'maxpages' => $maxpages
	    ])
	    <div style="text-align: center;">
	    	<button class="toggleSearch btn btn-default">Search</button>
	    </div>
		@endcomponent
		<div class="searcharea form-group">
			<input class="form-control" type="text" name="textsearch" id="text-search-input" placeholder="Body" />
			<select multiple class="form-control select2 tag-autocomplete-select" id="tag-search-input" name="search"></select>
			<br>
			<button class="btn btn-default submit-search">Go</button>
		</div>
    </div>
    
    @component('components.statusblock')
    @endcomponent

    <div>
    	<div class='row'>
    		<div class='col-sm-12'>
		    	@foreach($favours as $fav)
					@favour([
						'favour' => $fav,
						'belongsToUser' => ($fav['account']['accountId'] == $currentUser)
					])
					@endfavour
		    	@endforeach
		    </div>
    	</div>
    	@component('components.pagebuttons', [
    		'page' => $page,
    		'maxpages' => $maxpages
    	])
	    @endcomponent
    </div>
@endsection