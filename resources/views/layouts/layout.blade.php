@section('head')
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="/css/all.css">
	<script type="text/javascript" src="/js/all.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>@yield('title', 'uvoid')</title>
	@stack('extrahead')
	<script type="text/javascript">
		$(function() {

			$.ajaxSetup({
			    headers: {
				        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$('.pagebtn').click(function(e) {
				var pnum = $(this).attr('rel');
				
				if (pnum == -1) {
					return false;
				}

				var url = new URL(location.href);
				url.searchParams.set('page', pnum);
				location.href = url.href;
			});

			$('.remove-listing').click(function() {
				var fid = $(this).attr('rel');
				deleteWarning(function() {
					$.post('/favour/delete-favour/' + fid, function(res) {
						if (res == 'success') {
							removeFavourDiv('#favour' + fid, 'Deleted');
						}
					});
				});
			});

			$('.confirm-listing').click(function() {
				var fid = $(this).attr('rel');
				deleteWarning(function() {
					$.post('/favour/confirm-favour/' + fid, function(res) {
						if (res == 'success') {
							console.log(res);
							removeFavourDiv('#favour' + fid, 'Confirmed');
						}
					})
				}, 'When you confirm tokens are released from escrow')
			});

			function removeFavourDiv(target, message) {
				$(target)
					// We want to preserve favour height when deleting

					.css({
						'min-height': function() { return $(this).css('height') },
						'text-align' : 'center'
					})
					.html('<h1 style="display: inline-block; padding-top: 7rem;">[ ' + message +' ]</h1>')
					.fadeOut(500);
			}
		});
	</script>
</head>
<body>
    <div class="container">
    	<div class='row'>
    		<div class='col-sm-10 col-md-offset-1'>
	    		<div class='col-sm-4' id='user'>
	    			{{-- @if(session()->has('account'))
						@component('components.account-header', [
							'account' => session('account')
						])
						@endcomponent
					@endif --}}
	    		</div>

	    		<div class='col-sm-4'>
	    			@component('components.logo')
	    			@endcomponent
	    			<p class='intro'>uvoid is a simple platform where users can securely trade favours with anyone in the world</p>
	    		</div>

	    		<div class='col-sm-4' id='tokens'>
	    			@if(session()->has('account'))
	    			@endif
	    		</div>
	    	</div>
	    </div>
    	<div class='row'>
    		<div class='col-sm-10 col-md-offset-1'>
		    	@if(session()->has('account'))
		    		@component('components.topbar')
				    @endcomponent
		    	@endif
				@show
			</div>
		</div>
    </div><!-- /.container -->
</body>
</html>