@extends('layouts.layout')
@section('head')
	@parent
    <div class='row'>
        <div class='col-sm-4 col-md-offset-4'>
            <form method='POST' name='registerForm' action='/register'>
                @if(session('status'))
                    <blockquote>
                        <p>{{ session('status') }}</p>
                    </blockquote>
                @endif

                {{ csrf_field() }}

                <div class='form-group row'>
                    <div class='col-sm-12'>
                        <input name='user' type='text' class='form-control' id='user' readonly value='{{ $user }}'>
                    </div>
                </div>
                <div class='form-group row'>
                    <div class='col-sm-12'>
                        <input name='password' data-toggle='tooltip' data-html="true" title="8-32 Characters, must contain: <div>- 1 Uppercase</div><div>- 1 Lowercase</div><div>- 1 Number</div>" type='password' class='form-control' id='inputPassword' placeholder='password'>
                    </div>
                </div>

                <div class='form-group row'>
                    <div class='col-sm-12'>
                        <input name='confirmPassword' type='password' class='form-control' id='confirmInputPassword' placeholder='password'>
                    </div>
                </div>

                <div class='form-group row'>
                    <div class='col-sm-8'>
                        <a href='/login'>Back</a>
                    </div>
                    <div class='col-sm-4'>
                        <div class='text-right'>
                            <button name="bLogin" onclick="event.preventDefault(); if (checkFields([document.registerForm.password, document.registerForm.confirmPassword])) {document.registerForm.submit();}" class="btn btn-default" id="bLogin">Register</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection