let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
	.styles([
		// Vendor

	    'public/css/vendor/milligram.css',
	    'public/css/vendor/sweetalert2.min.css',
	    'public/css/vendor/select2.min.css',
	    'public/css/vendor/raleway.css',
	    'public/css/vendor/select2-bootstrap.min.css',
	    'public/css/app.css',

	    // Our CSS

	    'public/css/main.css'
], 'public/css/all.css')
	.scripts([
		// Vendor

		'public/js/vendor/jquery.min.js',
		'public/js/vendor/sweetalert2.min.js',
		'public/js/vendor/select2.min.js',
		'public/js/vendor/bootstrap.min.js',

		// Our modules

		'public/js/modules/selectHelper.js',
		'public/js/modules/formHelper.js',
		'public/js/modules/alertHelper.js'
	], 'public/js/all.js');