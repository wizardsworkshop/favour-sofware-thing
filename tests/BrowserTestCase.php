<?php

namespace Tests;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BrowserTestCase extends TestCase
{
	public function setUp()
	{
		parent::setUp();
	}
	
	protected function getSuccessResponse($url)
    {
        $response = $this->get($url);
        $response->assertSuccessful();

        return $response;
    }
}
