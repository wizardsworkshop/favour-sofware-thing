<?php

namespace Tests;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Support\Facades\DB;

use App\Account;

use App\Helpers\Users\UserManager;
use App\Helpers\Messages\MessageHandler;

class LoggedInTestCase extends BrowserTestCase
{
	// Account instance

	protected $account;
	protected $sessionParams;

	public function setUp()
	{
		parent::setUp();
	}

	protected function getSuccessResponse($url, $withAccount = false)
    {
    	if ($withAccount) {
    		$this->login();
    		$response = $this->withSessionAccount()->get($url);
    	} else {
    		$response = $this->get($url);
    	}

        $response->assertSuccessful();

        $this->destroy();

        return $response;
    }

    public function tearDown()
    {
    	parent::tearDown();
    }

    protected function login()
    {
    	$this->account = $this->getNewAccount();
		$this->sessionParams = [
			'account' => $this->account,
			'accountId' => $this->account->accountId
		];

		set_session_account($this->account);
    }

    protected function destroy()
    {
    	if ($this->account) {
            $this->clearMessages($this->account);

    		UserManager::deleteAccount();
    	}

    	session()->flush();
    }

    protected function getNewAccount()
    {
        return UserManager::createAccount(UserManager::getNewUsername(), 'ValidPass123');
    }

    protected function cleanupAccount(Account $account)
    {
        $this->clearMessages($account);
        UserManager::deleteAccount($account->accountId);
    }

    public function withSessionAccount()
    {
    	return $this->withSession($this->sessionParams);
    }

    protected function sendCurrentAccountMessage($text)
    {
        $this->assertTrue(
            MessageHandler::sendMessage($this->account->accountId, $text, $this->account->accountId)
        );
    }

    protected function clearMessages(Account $recipient)
    {
        DB::delete("
            DELETE FROM
                messages
            WHERE
                recipient = ?
        ", [$recipient->accountId]);
    }
}
