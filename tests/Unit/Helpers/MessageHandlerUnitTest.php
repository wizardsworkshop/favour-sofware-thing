<?php

namespace Tests\Unit\Helpers;

use Tests\LoggedInTestCase;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Helpers\Messages\MessageHandler;

class MessageHandlerUnitTest extends LoggedInTestCase
{
    public function testMessageHandlerSend()
    {
        $this->login();

        $this->sendCurrentAccountMessage('This is a message text');

        $this->assertDatabaseHas('messages', [
        	'recipient' => $this->account->accountId,
        	'sender' => $this->account->accountId
        ]);

        $this->destroy();
    }

    public function testMessageHandlerGetInbox()
    {
    	$this->login();

        $this->assertEmpty(MessageHandler::getInbox($this->account->accountId));
        	
        $this->sendCurrentAccountMessage('This is a message text');
        $this->sendCurrentAccountMessage('This is another message text');

        $this->assertCount(2, MessageHandler::getInbox($this->account->accountId));

    	$this->destroy();
    }
}
