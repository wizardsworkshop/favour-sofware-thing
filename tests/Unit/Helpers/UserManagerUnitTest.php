<?php

namespace Tests\Unit\Helpers;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

use App\Helpers\Users\UserManager;

class UserManagerUnitTest extends TestCase
{
	public function setUp()
	{
		parent::setUp();
	}

    public function testRandomUsername()
    {
    	$passes = [];
    	$i = 0;

    	while ($i++ < 20000) {
    		$passes[] = UserManager::getNewUsername();
    	}

    	// Check for duplicate values

    	$this->assertTrue(count(array_unique($passes)) == count($passes), 'Password generation not random enough!');
    }

    public function testValidatePassword()
    {
		$passes = [
			// Valid passwords
			['GoodPass123', 'AnotherGoodPass1', 'ThisPasswordIs9933MuchLonger', 'Pa$$w1thSp3£halCh4rs', '123StartsWithNums'],

			// Invalid passwords
			['BadPassNoNumbers', 'thissrt', 'thisoneonlylowercase12', 'THISISUPPERCASE123', 'ThisWouldBeAGoodPa55WordButItsActuallyTooLong']
		];

		foreach ($passes as &$pg) {
			foreach ($pg as &$pass) {
				$pass = UserManager::validatePassword($pass);
			}
		}

		$this->assertEquals($passes, [
			[true, true, true, true, true],
			[false, false, false, false, false]
		]);
    }

    public function testDeleteAccount()
    {
    	$account = $this->getDefaultAccount();

    	$userSearch = [
    		'username' => $account->username,
    		'accountId' => $account->accountId
    	];

    	$this->assertDatabaseHas('accounts', $userSearch);

    	$this->cleanupAccount();

    	$this->assertDatabaseMissing('accounts', $userSearch);
    }

    public function testChangePassword()
    {
    	$account = $this->getDefaultAccount();

		UserManager::changeCurrentAccountPassword('ValidNewPass123');

		$realpass = DB::select("
			SELECT
				password
			FROM
				accounts
			WHERE
				username = ?
		", [$account->username])[0]->password;

		$this->assertFalse(Hash::check('ValidPass123', $realpass), 'Password has not changed');
		$this->assertTrue(Hash::check('ValidNewPass123', $realpass), 'Error checking password hash');

    	$this->cleanupAccount();
    }

    private function getDefaultAccount()
    {
    	$account = UserManager::createAccount(UserManager::getNewUsername(), 'ValidPass123');

    	set_session_account($account);
    	return $account;
    }

    private function cleanupAccount()
    {
    	UserManager::deleteAccount();

    	session()->flush();
    }
}
