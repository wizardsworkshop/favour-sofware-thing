<?php

namespace Tests\Feature;

use Tests\LoggedInTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Helpers\Messages\MessageHandler;

class MessagePageTest extends LoggedInTestCase
{
    public function testBlankMessagePageLoads()
    {
        $response = $this->getSuccessResponse('/messages/inbox', true);

        $response->assertSee('You have no messages');
    }

    public function testMessagePageDisplaysRecords()
    {
    	$this->login();

        $this->sendCurrentAccountMessage('This is a message text');

        $response = $this->withSessionAccount()->get('/messages/inbox');

        $response->assertSuccessful();

        $response->assertSee('This is a message text');
        $response->assertSee('Reply');

        $response->assertSee($this->account->username);
        $response->assertSee(date('Y-m-d'));

    	$this->destroy();
    }
}
