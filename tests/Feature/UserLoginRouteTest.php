<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Helpers\Users\UserManager;

class UserLoginRouteTest extends TestCase
{

	// Matching password requirements (1 lower / uppercase, 1 number, {8,32})

	public $validPass = 'GoodPassword123';

	// Fails password requirements

	public $invalidPass = 'BadPassword';

    public function testUserCreateSuccess()
    {
        $response = $this->getLoginTest(
        	$this->validPass, 
        	$this->validPass
    	);

        $response
        	->assertStatus(302)
        	->assertRedirect('/home/index');

        // Cleanup

        UserManager::deleteAccount();
    }

    public function testUserCreatePasswordMismatch()
    {
    	$response = $this->getLoginTest(
    		$this->validPass, 
    		$this->invalidPass
    	);

    	$this->assertFailCreate(
    		$response, 
    		'Confirmation password does not match'
    	);	
    }

    public function testUserCreatePasswordFailRequirements()
    {
    	$response = $this->getLoginTest(
    		$this->invalidPass,
    		$this->invalidPass
    	);

    	$this->assertFailCreate(
    		$response,
    		'Password does not meet requirements'
    	);
    }

    // Wrapper to get login response instance with
    // 2 password field values

    private function getLoginTest($password, $confirmPassword)
    {
    	return $this->json('POST', '/register', [
            'user' => UserManager::getNewUsername(),
        	'password' => $password,
        	'confirmPassword' => $confirmPassword
        ]);
    }

    // Ensures account creation was not successful and 
    // redirect back was sent with given status

    private function assertFailCreate(&$response, $status)
    {
    	$response
    		->assertStatus(302)
    		->assertRedirect('/register')
    		->assertSessionHas('status', $status);
    }
}
