<?php

namespace Tests\Feature;

use Tests\LoggedInTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccountPageTest extends LoggedInTestCase
{
    public function testAccountPageLoads()
    {
    	$this->login();

        $response = $this->withSessionAccount()->get('/home/account');

    	$response->assertSuccessful();

        $response->assertSeeText('Username');
        $response->assertSeeText('Tokens');
        $response->assertSeeText('Change Password');
        $response->assertSeeText('Delete Account');

        $response->assertSeeText(session('account')->username);

        $this->destroy();
    }

    public function testChangePasswordJson()
    {
    	$this->login();

    	$response = $this->accountPost([
    		'password' => 'NewPass123',
    		'confirmpassword' => 'NewPass123'
    	]);

    	$response->assertSessionHas('status', 'Password Updated');

    	$response = $this->accountPost([
    		'password' => 'OldPass123',
    		'confirmpassword' => 'NewPass123'
    	]);

    	$response->assertSessionHas('status', 'Confirmation password does not match');

    	$response = $this->accountPost([
    		'password' => 'NewPassInvalid',
    		'confirmpassword' => 'NewPassInvalid'
    	]);

    	$response->assertSessionHas('status', 'Password does not meet requirements');

    	$this->destroy();
    }

    private function accountPost($data)
    {
		$response = $this->withSessionAccount()->post('/home/account', $data);
    	$response->assertStatus(302);

    	return $response;
    }
}
