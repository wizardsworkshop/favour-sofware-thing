<?php

namespace Tests\Feature;

use Tests\BrowserTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginAreaTest extends BrowserTestCase
{

	// Basic functions to test pages load 

    public function testHomeLoads()
    {
    	$response = $this->getSuccessResponse('/');

        $response->assertSeeText('uvoid');
    }

    public function testRegisterLoads()
    {
        $response = $this->getSuccessResponse('/register');

        $response->assertSeeText('Register');
    }

    public function testTermsLoads()
    {
    	$response = $this->getSuccessResponse('/terms');
    }

    public function testHomePageSessionGuarded()
    {
        session()->flush();

        $response = $this->get('/home/index');

        $response->assertRedirect('/');
    }
}
