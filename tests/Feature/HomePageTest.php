<?php

namespace Tests\Feature;

use Tests\LoggedInTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomePageTest extends LoggedInTestCase
{
    public function setUp()
    {
		parent::setUp();
    }

    // Make sure we clear session and redirect on logout

    public function tearDown()
	{
		$response = $this->get('/logout');

		$response->assertSessionMissing('account');
		$response->assertSessionMissing('accountId');

		$response->assertRedirect('/');

		parent::tearDown();
	}

    public function testHomePageLoads()
    {
    	$response = $this->getSuccessResponse('/home/index', true);

    	$response->assertSeeText('Search');
        $response->assertSeeText('Prev');
        $response->assertSeeText('Next');
        
        $response->assertViewHas('favours');
    }
}
