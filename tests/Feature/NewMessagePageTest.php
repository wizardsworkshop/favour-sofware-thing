<?php

namespace Tests\Feature;

use Tests\LoggedInTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NewMessagePageTest extends LoggedInTestCase
{
    public function testNewMessagePageLoads()
    {
    	$this->login();
    	$recipient = $this->getNewAccount();

    	$response = $this->withSessionAccount()->get('/messages/new-message/' . $recipient->accountId);

    	$response->assertSuccessful();
    	$response->assertSee('New Message');
    	$response->assertSee('Send');
    	$response->assertSee('To: ' . $recipient->username);

    	$this->cleanupAccount($recipient);
    	$this->destroy();
    }

    public function testNewMessageForm()
    {
    	$this->login();

    	$response = $this->withSessionAccount()->post('/messages/send-message/' . $this->account->accountId, [
    		'body' => 'This is the message body'
    	]);

    	$response->assertSuccessful();
    	$response->assertSee('success');

    	$this->assertDatabaseHas('messages', [
    		'recipient' => $this->account->accountId
    	]);

    	$this->destroy();
    }
}
