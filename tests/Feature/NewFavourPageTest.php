<?php

namespace Tests\Feature;

use Tests\LoggedInTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NewFavourPageTest extends LoggedInTestCase
{
    public function testNewFavourPageLoads()
    {
        $response = $this->getSuccessResponse('/favour/new-favour', true);

        $response->assertSee('New Favour');
        $response->assertSee('Save');
    }
}
